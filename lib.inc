section .text

%define SYS_EXIT 60
%define STDOUT_FD 1
%define SYS_WRITE 1
%define SYS_READ 0
%define STDIN_FD 0
%define MIN_LENGTH 2
%define BASE_TEN 10 
%define TRUE 1
%define FALSE 0
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	cmp byte [rdi + rax], 0 
	jz .stop
	inc rax ; increment counter until reach null-terminator
	jmp .loop
    .stop:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi ;save register before call
    call string_length ;length of string in rax
    ;Prepare syscall
    pop rsi ;put word in rsi reg
    mov rdx, rax ;put length of the word in rdx
    mov rdi, STDOUT_FD
    mov rax, SYS_WRITE
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, `\n`  ;symbol next line

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi ; push to mem to have pointer to our char
    mov rsi, rsp ; syscall accepts pointers so mov pointer to char into rsi reg
    pop rdi ; restore stack
    ;prepare for stdout
    mov rax, SYS_WRITE
    mov rdi, STDOUT_FD
    mov rdx, 1 ;length
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    mov rax, rdi
    test rax, rax ;if num is negative add - else just print as unsigned int
    jge .print_positive ;if >= 0 then print like unsigned int
    mov rdi, '-' ; if < 0 print -
    push rax ;save reg
    call print_char ;print -
    pop rax ;restore value
    neg rax ;twos complement
    .print_positive:
    	mov rdi, rax

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; divide: rax = rax / rdi,  rdx = rax % rdi
; to convert num to ascii need to divide it by 10, use remainder as a result, add 0
print_uint:
    xor rax, rax
    mov rax, rdi
    mov rdi, BASE_TEN ;set divisor
    mov rsi, rsp ;save stack pointer to resotre stack
    push 0 ; null-terminator
    .divide:
        xor rdx, rdx ; clear for remainder
        div rdi
        add dl, '0' ;make ascii code ;1-byte
        dec rsp
        mov [rsp], dl
        test rax, rax ; rax is 0?
        jnz .divide
    .print_result: ;getting chars from stack one by one
        mov rdi, rsp ;pointer to string for print_string
        push rsi ;save init state of sp
        call print_string
        pop rsi ;restore init state of sp
    mov rsp, rsi ;resotre stack pointer
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov al, byte [rdi]
	mov dl, byte [rsi]
    	cmp al, dl
	jne .not_equal ;if codes not equal then exit
	test al, al ;check for null-terminator
	je .equal ;if null then exit
	inc rdi ;move to next char
	inc rsi ;move to next char
	jmp .loop 
    .equal:
    	mov rax, TRUE
	jmp .exit
    .not_equal:
    	mov rax, FALSE
    .exit:
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    dec rsp ;make space for char
    mov rax, SYS_READ
    mov rdi, STDIN_FD
    mov rdx, 1
    mov rsi, rsp ;where to store read char
    syscall
    
    test rax, rax ;check if returend bytes are not zero (if not ctrl+d is pressed)
    jz .end_of_line
    mov al, byte [rsp] ;move byte to rax register from stack
    inc rsp ;return stack state
    jmp .exit
    .end_of_line:
    	xor rax, rax
	inc rsp
    .exit:
    	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;args: rdi = adress of buff, rsi = lenght of buff
;return: OK - rax = adress of buffer, rdx = length of string. NO - rax = 0
read_word:
    ; push callee save reg
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    cmp r14, MIN_LENGTH;min length
    jz .exit
    .whitespace_loop:
        call read_char ;reading first char
        test rax, rax ;rax = 0?
	jz .return ;if true => return
	cmp r14, r13 ;check buffer, if r13 >= r14 => overflow 
	jnl .overflow 
        cmp rax, ' '
        jz .skip_line
        cmp rax, `\n`
        jz .skip_line
        cmp rax, `\t`
        jz .skip_line
        mov byte [r12 + r14], al ;write into buffer
        inc r14 ; increment word length
        jmp .whitespace_loop 
    .skip_line:
        test r14, r14 ;r14 = 0?
        jz .whitespace_loop ;if true get next char
    .return:
        mov byte [r12 + r14], 0 ; add null terminator
        mov rax, r12 ;return address to buffer
        mov rdx, r14 ;return string length
        jmp .exit
    .overflow:
        xor rax, rax ;rax = 0
    .exit:
        pop r12
	pop r13
	pop r14
        ret
;Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi = pointer to a start of string
; mul reg = rax * reg
parse_uint:
    xor rax, rax
    xor rdx, rdx ;use as a counter
    xor r8, r8 ;buffer for chars
    .loop:
        mov r8b, byte [rdi + rdx] ;get first char
        test r8b, r8b ;check for null terminator
        jz .exit
        sub r8, '0' ;ascii -> int + flags
	cmp r8b, 9
	ja .not_digit
        imul rax, rax, BASE_TEN ;mul to shift left
        add rax, r8
        inc rdx
        jmp .loop
    .not_digit:
        test rax, rax
        jnz .exit ;if rax != 0 -> exit
        xor rdx, rdx ;clear counter
    .exit:
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r8, r8 ;buffer for chars
    xor rdx, rdx ;lenght counter
    mov r8b, byte [rdi + rdx]
    cmp r8b, '-'; if '-' -> parse as unsigned + neg
    jz .negative
    .positive:
    	call parse_uint
	ret
    .negative:
        inc rdi
	call parse_uint
	test rdx, rdx ;if failed => exit
	jz .exit
	inc rdx
	neg rax
    .exit:	
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; find length of string and compare it to buffer size
    push rdi ;ptr to string
    push rsi ;ptr to string buf
    push rdx ;string length
    call string_length ;return string length in rax
    pop rdx
    pop rsi
    pop rdi
    
    cmp rax, rdx 
    jg .overflow
    xor rax, rax ;counter 
    .make_copy:
        xor r8, r8
    	mov r8b, byte [rdi + rax] ;store char from source string
	mov byte [rsi + rax], r8b
	test r8b, r8b ;check for null terminator
	jz .exit
	inc rax
	jmp .make_copy
    .overflow:
    	xor rax, rax
    .exit:
    	ret
